/* window.vala
 *
 * Copyright 2021 favelop.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;

namespace simplelikert {
	[GtkTemplate (ui = "/org/gnome/simplelikert/window.ui")]
	public class Window : Gtk.ApplicationWindow {
	
	    [GtkChild]
	    Gtk.Grid box;
	    
	    [GtkChild]
	    Gtk.ScrolledWindow scroll;
	    
	    private List<List<Gtk.RadioButton>> allRadioButtons;
	    
	    private List<string> questions;
	    private List<string> answers;

		public Window (Gtk.Application app, string confFile, string questionsFile) {
			Object (application: app);
			
			this.destroy.connect(this.save);
			
			try {
			    var screen = this.get_screen ();
			    var css_provider = new Gtk.CssProvider();
			    css_provider.load_from_data(".mylabel{font-size: 16px}");
			    Gtk.StyleContext.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);
			} catch (Error e) {
			    
			}
			
			allRadioButtons = new List<List<Gtk.RadioButton>>();
			
			questions = readFileAsLines(questionsFile);
			answers = readFileAsLines(confFile);
			
			box.set_halign(Align.CENTER);
			
			box.attach(createFixedLabel(answers.nth_data(0)), 0, 0, (int) answers.length() - 1, 1);
			box.attach(new Gtk.Separator(Orientation.HORIZONTAL), 0, 1, (int) answers.length() - 1, 1);
			
			int i = 1;
			foreach (var question in questions) {
			    addRow(i, question);
			    i++;
			}
			
			Gtk.Button saveButton = new Gtk.Button.with_label("Speichern");
			
		    /*Gdk.RGBA color = Gdk.RGBA();
		    color.red = 0.8;
		    color.green = 0.85;
		    color.blue = 1;
		    color.alpha = 1;
		    saveButton.override_background_color(StateFlags.NORMAL, color);*/
		    
		    saveButton.get_style_context().add_class("suggested-action");
		    saveButton.clicked.connect(this.close);
			
			box.attach(saveButton, (int) answers.length() - 2, (int)(questions.length() + 1) * 3, 1, 1);
			
			this.show_all();
		}
		
		private void addRow(int rowNum, string question) {
		    int actualRow = rowNum * 3;
		    
		    Gtk.Label qlabel = createFixedLabel(question);
		    //qlabel.set_halign(Align.START);
		    box.attach(qlabel, 0, actualRow, (int) answers.length() - 1, 1);
            
            List<Gtk.RadioButton> radiogroup = new List<Gtk.RadioButton>();
            int i = 0;
            Gtk.RadioButton group = new Gtk.RadioButton(null); 
            Gtk.RadioButton rb = null;    
		    foreach (var answer in answers) {
		        //skip statement
		        if (i > 0) {
		            rb = new Gtk.RadioButton.with_label_from_widget(group, answer);
		            rb.set_mode(false);
		            var rb_label = rb.get_child() as Gtk.Label;
		            rb_label.set_line_wrap(true);
		            rb_label.set_justify(Justification.CENTER);
		            
		            rb.clicked.connect(() => scrollNext(rowNum));
		            
		            box.attach(rb, i - 1, actualRow + 1);
		            
		            radiogroup.append(rb);
		        }
		        i++;
		    }
		    
		    allRadioButtons.append((owned) radiogroup);
		    
		    box.attach(new Gtk.Separator(Orientation.HORIZONTAL), 0, actualRow + 2, (int) answers.length() - 1, 1);
		}
		
		private Gtk.Label createFixedLabel(string text) {
		    Gtk.Label result = new Gtk.Label(text);
		    //result.set_width_chars(64);
		    result.set_alignment(0.5f, 0.5f);
		    result.set_line_wrap(true);
		    result.get_style_context().add_class("mylabel");
		    
		    return result;
		}
		
		private static List<string> readFileAsLines(string filepath) {
	    	File file = File.new_for_path(filepath);
	    	string line ="";
	    	List<string> result = new List<string>();
	    	
	        try {
		        FileInputStream @is = file.read ();
		        DataInputStream dis = new DataInputStream(@is);
		        
		        while ((line = dis.read_line ()) != null) {
			        result.append(line);
		        }
	        } catch (Error e) {
		        print ("Error: %s\n", e.message);
	        }

            return result;
		}
		
		private void scrollNext(int index) {
		    index = index - 1;
		    int maxIndex = (int) allRadioButtons.length() - 1;
		    
		    if (index <= maxIndex) {
		        var nextRB = allRadioButtons.nth_data(index).nth_data(0);
		        Allocation alloc;
		        nextRB.get_allocation(out alloc);
		        var y = alloc.y - 48; // respect label on top of rb
		        animatedScrollTo(y);
		        
		        // without scroll:
		        //var adj = scroll.get_vadjustment();
                //adj.set_value(y);
		    }
		}
		
		// actually prints
		private void save() {
		    int row = 0;
		    foreach (unowned List<Gtk.RadioButton> radiogroup in allRadioButtons) {
		        int col = 0;
		        int row_res = 0; // resolution of row
		        foreach (unowned Gtk.RadioButton radiobutton in radiogroup) {
		            if (radiobutton.get_active()) {
		                row_res = col;
		                break;
		            }
		            col++;
		        }
                print("%d,%d\n", row, row_res);
		        row++;
		    }
		}
		
		// from https://blogs.gnome.org/jsparber/2018/04/29/animate-a-scrolledwindow/
		private void animatedScrollTo(double scrollPosition) {
            var adj = scroll.get_vadjustment();
            var clock = scroll.get_frame_clock();
            int duration = 200;
            double start = adj.get_value();
            double end = scrollPosition;
            double start_time = clock.get_frame_time();
            double end_time = start_time + 1000 * duration;
            scroll.add_tick_callback(() => {
                var now = clock.get_frame_time();
                if (now < end_time && adj.get_value() != end) {
                    double t = (now - start_time)/ (end_time - start_time);
                    t = ease_out_cubic(t);
                    adj.set_value(start + t * (end - start));
                    return GLib.Source.CONTINUE;
                }
                else
                {
                    adj.set_value(end);
                    return GLib.Source.REMOVE;
                }
            });
        }
        
        /* From clutter-easing.c, based on Robert Penner's
         * infamous easing equations, MIT license.
         */
        private double ease_out_cubic(double t) {
          double p = t - 1;
          return p * p * p + 1;
        }
	}
	
}
