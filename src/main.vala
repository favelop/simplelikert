/* main.vala
 *
 * Copyright 2021 favelop.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

int main (string[] args) {
    var app = new SurveyApplication();

	return app.run(args);
}

public class SurveyApplication : Gtk.Application {
    private string confFile;
    private string questionsFile;

	public SurveyApplication() {
		Object(application_id: "com.fas.simplelikert", flags: ApplicationFlags.HANDLES_COMMAND_LINE);
		set_inactivity_timeout (500);
		confFile = "";
		questionsFile = "";
	}

	public override void activate() {
		var win = this.active_window;
		if (win == null) {
			win = new simplelikert.Window(this, confFile, questionsFile);
		}
		win.set_position(Gtk.WindowPosition.CENTER_ALWAYS);
		win.present();
	}

	public override int command_line(ApplicationCommandLine command_line) {

		// keep the application running until we are done with this commandline
        string[] args = command_line.get_arguments();
        if (args.length > 2) {
            confFile = args[1];
            questionsFile = args[2];
            
            activate();  
        } else {
             print ("Not enough parameters!\nUsage: simplelikert CONF_FILE QUESTIONS_FILE\n"); 
        }

		return 0;
	}
}
