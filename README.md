# Simple Likert

![Screenshot](simple-likert.png)

Simple survey application with a Likert scale that can be used in automated studies.
It outputs the answers in CSV format to the standard output.

## Building

    mkdir build
    meson build
    cd build
    ninja

## Usage
    
    ./simplelikert config questions

**Config**: Each line the answer on the Likert scale.

**Questions**: Each line a question.